#include "sync.h"
#include "syscalls.h"

//Creates a mutex with said name if it doesn't exist, otherwise it just returns it
mutex getMutex(const char * name) {
	return os_getMutex(name);
}

//Tries to adquire a mutex
void wait(mutex m) {
	os_wait(m);
}

//Releases a mutex
void post(mutex m) {
	os_post(m);
}

//Deletes a mutex and unblocks all the processed attached to it
void deleteMutex(mutex m) {
	os_deleteMutex(m);
}

