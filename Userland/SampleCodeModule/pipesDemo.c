#include "pipeDemo.h"
#include "string.h"
#include "syscalls.h"
#include "stdio.h"
#include "pipes.h"

int running;

void initPipesDemo(){
	running = 1;

	introduction();

	char c;
	while (running){
		c = getChar();
		if (c == 'q'){
			running = 0;
		} else if (c == 'm'){
			createMessengers();
		}
	}
	closePipes(PIPE1, PIPE2);

	return;
}

void introduction(){
	printf("\nThis application will create two simple processes that use a pair of pipes to communicate with each other!\n");
	printf("Press m to send the messages\n");
	printf("Press q to quit at any time\n");
	return;
}

void createMessengers(){
	os_create("firstMessenger", (uint64_t)&messenger1, 15, 4096, 0);
	os_create("secondMessenger", (uint64_t)&messenger2, 15, 4096, 0);
	return;
}

void messenger1(){
	int fd[2];
	char buffer[BUF_SIZE];
	// Messenger1 reads from pipe2
	fd[0] = open(PIPE2);
	
	// This happened because PIPE2 does not exist and there is no more memory to create it
	if (fd[0] == (uint64_t)-1) {
		printf("\nOut of memory to create PIPE2\n");
	}

	// Messenger1 writes from pipe1
	fd[1] = open(PIPE1);

	if (fd[1] == (uint64_t)-1) {
		printf("\nOut of memory to create PIPE1\n");
	}


	char * message = "Messenger 1 says hello to messenger 2!\n";


	printf("Messenger 1 writing to PIPE1!\n");
	write(fd[1], message, strlen(message) + 1);

	printf("Messenger 1 reading from PIPE2\n");
	int bytesRead = read(fd[0], buffer, BUF_SIZE);

	buffer[bytesRead] = '\0';

	printf("Message messenger 1 recieved from messenger 2: ");
	printf(buffer);
	printf("\n");
}

void messenger2(){
	int fd[2];
	char buffer[BUF_SIZE];
	// Messenger2 reads from pipe1
	fd[0] = open(PIPE1);
	
	// This happened because PIPE1 does not exist and there is no more memory to create it
	if (fd[0] == (uint64_t)-1) {
		printf("\nOut of memory to create PIPE1\n");
	}

	// Messenger2 writes from pipe2
	fd[1] = open(PIPE2);

	// This happened because PIPE2 does not exist and there is no more memory to create it
	if (fd[1] == (uint64_t)-1) {
		printf("\nOut of memory to create PIPE2\n");
	}

	char * message = "Messenger 2 says hello to messenger 1!\n";


	printf("Messenger 2 writing to PIPE2!\n");
	write(fd[1], message, strlen(message) + 1);

	printf("Messenger 2 reading from PIPE1\n");
	int bytesRead = read(fd[0], buffer, BUF_SIZE);

	buffer[bytesRead] = '\0';

	printf("Message messenger 2 recieved from messenger 1: ");
	printf(buffer);
	printf("\n");
}

void closePipes(char * name1, char * name2){
	close(name1);
	close(name2);
}