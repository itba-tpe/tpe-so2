#include "prodcons.h"
#include "pipes.h"
#include "sync.h"
#include "stdio.h"
#include "string.h"
#include "syscalls.h"
#include <stdint.h>


int run;
int prodIndex;
int prodAmount;
int prodAmountWaiting;
int consIndex;
int consAmount;
int consAmountWaiting;

static mutex lock;
static mutex prodWaiting;
static mutex consWaiting;
static uint64_t fdPipe;

void initProdCons(){
	run = 1;

	intro();
	resetVariables();

	lock = getMutex("prodConsMutex");
	prodWaiting = getMutex("prodMutex");
	consWaiting = getMutex("consMutex");

	fdPipe = open(PIPE);
	
	char c;
	while (run){
		c = getChar();
		if (c == 'q'){
			run = 0;
		} else if (c == 'p'){
			createProducer();
		} else if (c == 'c'){
			createConsumer();
		}
	}

	deleteMutex(lock);
	deleteMutex(prodWaiting);
	deleteMutex(consWaiting);
	close(PIPE);
	return;
}

void intro(){
	printf("\nApplication will create producers and consumer that wish to access the same pipe\n");
	printf("Press p to create a producer\n");
	printf("Press c to create a consumer\n");
	printf("Press q to quit at any time\n");
	printf("\n");
	return;
}

void createProducer(){
	wait(lock);
	if (prodIndex < MAX_PROD){
		printf("Producer #");
		printInt(++prodIndex);
		printf(" created\n");
		post(lock);
		os_create("producer", (uint64_t)&producer, PRIORITY, MEM_SPACE, BACKGROUND);
	} else {
		post(lock);
		printf("Maximum amount of producers reached\n");
	}
	return;
}

void createConsumer(){
	wait(lock);
	if (consIndex < MAX_CONS){
		printf("Consumers remaining = ");
		printInt(++consIndex);
		printf("\n");
		post(lock);
		os_create("consumer", (uint64_t)&consumer, PRIORITY, MEM_SPACE, BACKGROUND);
	} else {
		post(lock);
		printf("Maximum amount of consumers reached\n");
	}
	return;
}

void producer() {
	wait(lock);

	char * msg = "This message is from the producers and will be read by the consumers";

	while (prodAmount != 0){
		prodAmountWaiting++;
		post(lock);
		// If there is another process waiting to write, the process will block here
		wait(prodWaiting);
		wait(lock);
	}
	if (prodAmountWaiting > 0) prodAmountWaiting--;
	prodAmount++;
	post(lock);
	
	write(fdPipe, msg, strlen(msg) + 1);
	printf("Producer wrote: ");
	printf(msg);
	printf("\n\n");

	wait(lock);
	prodAmount--;
	prodIndex--;
	post(lock);

	if(consAmountWaiting > 0){
		// If there were consumers waiting, unblock them all so they can consume
		for(int i = consAmountWaiting; i > 0; i--){
			post(consWaiting);
		}
	} else {
		// If there are any producers waiting, unblock the first one
		if (prodAmountWaiting > 0){
			post(prodWaiting);
		}
	}
	return;
}

void consumer() {
	char buff[25];

	wait(lock);
	while (prodAmount != 0){
		consAmountWaiting++;
		post(lock);
		wait(consWaiting);
		wait(lock);
	}
	if(consAmountWaiting > 0) consAmountWaiting--;
	consAmount++;
	post(lock);

	read(fdPipe, buff, 24);
	buff[25] = '\0';
	printf("Consumer read: ");
	printf(buff);
	printf("\n\n");

	wait(lock);
	consAmount--;
	consIndex--;
	post(lock);

	//If there are no consumers and ther ARE producers waiting, unblock a producer
	if(consAmount == 0 && prodAmountWaiting != 0){
		post(prodWaiting);
	}

	return;
}

void resetVariables(){
	run = 1;
    prodIndex = 0;
	prodAmount = 0;
	prodAmountWaiting = 0;
	consIndex = 0;
	consAmount = 0;
	consAmountWaiting = 0;
}
