GLOBAL read
GLOBAL write
GLOBAL os_time
GLOBAL os_clear
GLOBAL os_draw
GLOBAL os_ticks
GLOBAL os_sec
GLOBAL os_beep
GLOBAL os_unbeep
GLOBAL os_malloc
GLOBAL os_free
GLOBAL os_create
GLOBAL os_kill
GLOBAL os_wait
GLOBAL os_post
GLOBAL os_getMutex
GLOBAL os_deleteMutex
GLOBAL os_ps
GLOBAL os_open_pipe
GLOBAL os_close_pipe
GLOBAL os_get_pid

section .text

%macro start 0
	push rbp
	mov rbp, rsp

	push rbx
	push rcx
	push r12
	push r13
	push r15
%endmacro

%macro finish 0
	pop r15
	pop r13
	pop r12
	pop rcx
	pop rbx

	mov rsp, rbp
	pop rbp
	ret
%endmacro


os_ticks:
	start

	mov rdi, 0x01
	mov rsi, aux

	int 80h

	finish

os_sec:
	start

	mov rdi, 0x02
	mov rsi, aux

	int 80h

	finish

read:
	start

	; CAME AS: RDI --> file descriptor, RSI --> buffer, RDX --> bytes to read

	mov rcx, rdx
	mov rdx, rsi
	mov rsi, rdi
	mov rdi, 0x03

	; CALL SYSCALL AS: RDI --> SysCode RSI --> file descriptor, RDX --> buffer, RCX --> bytes to read

	int 80h

	; In RAX returns the amount of bytes read and in the buffer are the characters read

	finish

write:
	start

	; CAME AS: RDI --> file descriptor, RSI --> buffer, RDX --> bytes to write

	mov rcx, rdx
	mov rdx, rsi
	mov rsi, rdi
	mov rdi, 0x04

	; CALL SYSCALL AS: RDI --> SysCode RSI --> file descriptor, RDX --> buffer, RCX --> bytes to write

	int 80h

	; In RAX returns the amount of bytes written

	finish

os_time:
	start

	mov rdi, 0x05
	mov rsi, timeArray

	; CALL SYSCALL AS: RDI --> SysCode , RSI --> buffer to store time

	int 80h

	; In RAX returns the the string with the current time

	finish

os_clear:
	start

	mov rdi, 0x06

	int 80h

	finish


os_draw:
	start

	; CAME AS: RDI --> x , RSI --> y, RDX --> r, RCX --> g, R8 --> b

	mov r9, r8
	mov r8, rcx
	mov rcx, rdx
	mov rdx, rsi
	mov rsi, rdi
	mov rdi, 0x07

	; CALL SYSCALL AS: RDI --> SysCode , RSI --> x, RDX --> y, RCX --> r, R8 --> g, R9 -->b

	int 80h

	finish

os_beep:
	start

	mov rdi, 0x08

	int 80h

	finish

os_unbeep:
	start

	mov rdi, 0x09

	int 80h

	finish

os_malloc:
	start

	; CALL SYSCALL AS: RDI --> SysCode , RSI --> memSize

	mov rsi, rdi
	mov rdi, 0x0A

	int 80h

	finish

os_free:
	start

	; CALL SYSCALL AS: RDI --> SysCode , RSI --> memPtr

	mov rsi, rdi
	mov rdi, 0x0B

	int 80h

	finish

os_create:
	start

	; CALL SYSCALL AS: RDI --> SysCode , RSI --> processName, RDX --> functionRIP, RCX --> priority, 
	; R8 --> resMemory, r9 --> foreground

	mov r9, r8
	mov r8, rcx
	mov rcx, rdx
	mov rdx, rsi
	mov rsi, rdi
	mov rdi, 0x0C

	int 80h

	finish

os_kill:
	start

	; CALL SYSCALL AS: RDI --> SysCode, RSI --> pid

	mov rsi, rdi
	mov rdi, 0x0D

	int 80h

	finish

os_wait:
	start

	; CALL SYSCALL AS: RDI --> SysCode, RSI --> mutex

	mov rsi, rdi
	mov rdi, 0x0E

	int 80h

	finish

os_post:
	start

	; CALL SYSCALL AS: RDI --> SysCode, RSI --> mutex

	mov rsi, rdi
	mov rdi, 0x0F

	int 80h

	finish

os_getMutex:
	start

	; CALL SYSCALL AS: RDI --> SysCode, RSI --> name

	mov rsi, rdi
	mov rdi, 0x10

	int 80h

	finish

os_deleteMutex:
	start

	; CALL SYSCALL AS: RDI --> SysCode, RSI --> mutex

	mov rsi, rdi
	mov rdi, 0x11

	int 80h

	finish

os_ps:
    start

    mov rdi, 0x12
    mov rsi, plist

    int 80h

    finish

os_open_pipe:
	start

	; CALL SYSCALL AS: RDI --> SysCode, RSI --> pipeName

	mov rsi, rdi
	mov rdi, 0x13

	int 80h

	finish
	
os_close_pipe:
	start

	; CALL SYSCALL AS: RDI --> SysCode, RSI --> pipeName

	mov rsi, rdi
	mov rdi, 0x14

	int 80h

	finish


section .data
	timeArray times 6 DW 0

section .bb
	aux resb 8	; para enteros
    plist resb 64 ; to save process list
