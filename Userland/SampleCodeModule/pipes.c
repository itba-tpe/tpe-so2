#include "pipes.h"

uint64_t open(char * name){
	return os_open_pipe(name);
}

void close(char * name){
	os_close_pipe(name);
}