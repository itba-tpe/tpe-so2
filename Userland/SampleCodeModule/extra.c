#include "sync.h"
#include "stdio.h"

void whileOne(){
	printf("\nProgram eternally loops\n");
	while (1){
		//DO ABSOLUTELY NOTHING
	}
}

//Program will block and will stay blocked eternally
void doubleLock(){
	printf("\nProgram will wait twice on the same mutex, locking it\n");
	mutex m = getMutex("doubleLock");
	wait(m);
	wait(m);
}