#include "include/modules.h"
#include "include/syscalls.h"
#include "include/stdio.h"
#include "include/vidio.h"
#include "include/extra.h"
#include "include/testInvalidOp.h"
#include "include/sync.h"

void help() {
	printf("\nHelp Manual for TPEG2");
	printf("\nclear:");
	printf("\n\tIt will clear the screen");
    printf("\nps:");
    printf("\n\tshows information about all running processes");
	printf("\ntime:");
	printf("\n\tIt will display on screen the current time in hh:mm:ss format");
	printf("\npong:");
	printf("\n\tIt will launch the game Pong");
	printf("\n\tUse WS and IK to move up and down the paddles. CTRL + c to exit the game.");
	printf("\npipeDemo:");
	printf("\n\tIt will launch a program that demonstrates two processes interacting.");
	printf("\nmemoryDemo:");
	printf("\n\tIt will launch a program that allocates memory and verifies that no pointers overlap.");
	printf("\nprodcons:");
	printf("\n\tIt will launch a producer/consumer program that utilizes pipes and mutexes.");
	printf("\nwhileOne:");
	printf("\n\tIt will launch a program that loops eternally with a while(1)");
	printf("\ndoubleLock:");
	printf("\n\tIt will launch a program that blocks itself by using a mutex.");
	printf("\nexit:");
	printf("\n\tcloses the terminal");
}

void testForeground() {
	printf("Starting...\n");
	printf("Getting mutex...\n");
	mutex foreground = getMutex("foreground");
	printf("Calling wait...\n");
	wait(foreground);
	printf("Returning from wait\n");
	printf("Calling post...\n");
	post(foreground);
	printf("Ending...\n");
}

void showTime() {
	printf("\nCurrent time = ");
	uint64_t * currTime = os_time();		//returns an array with the current hour, minutes, and seconds

	char time[9];					//pass the array to a string to print
	time[0] = currTime[0] + '0';
	time[1] = currTime[1] + '0';
	time[2] = ':';
	time[3] = currTime[2] + '0';
	time[4] = currTime[3] + '0';
	time[5] = ':';
	time[6] = currTime[4] + '0';
	time[7] = currTime[5] + '0';
	time[8] = 0;

	printf(time);
}


int div(int n) {
	return 10/n;
}

//Creado para conocer la cantidad de ticks por segundo para que aunque el timer tick sea 
//distinto, el pong funcione con la misma velocidad. No logramos hacer que funcione bien por varias 
//razones entonces vamos a tomar 18 ticks por segundo por como esta definido en el kernel space 

int ticksPerSecond() {
	return 18;
/*
	int secondsTotal;
	int ticksTotal;
	double i = 0;
	int ticksStart = os_ticks();
	int secondStart = os_sec();

	do {
		while(i < 100000){		//HORRIBLE VER SI SE PUEDE HACER DE OTRA MANERA
			i += 0.1;
		}
	while((secondsTotal = os_sec() - secondStart) == 0);
	ticksTotal = os_ticks() - ticksStart;
	return ticksTotal/secondsTotal;
	*/
}

void exceptionDiv0() {
	printf("\nDividing by zero");
	div(0);
}

void exceptionInvalidOp() {
	printf("\nAttempting a jmp 0x0");
	invalidOpTest();
}

void clear() {
	os_clear();
}

void ps(){
    char * plist = (char *) os_ps();
    printf(plist);
}
