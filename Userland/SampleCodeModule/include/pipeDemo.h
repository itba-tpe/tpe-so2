#ifndef PIPEDEMO_H
#define PIPEDEMO_H

#define BUF_SIZE 50
#define PIPE1 "pipe1"
#define PIPE2 "pipe2"

void initPipesDemo();
void introduction();
void createMessengers();
void messenger1();
void messenger2();
void closePipes();

#endif