#ifndef MEMORYDEMO_H
#define MEMORYDEMO_H

#define MEM_ALLOCATED_AMOUNT 50
#define INITIAL_MEM 4000
#define OFFSET 2000

void memoryDemo();

#endif