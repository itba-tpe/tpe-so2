#ifndef STRING_H
#define STRING_H

int strcmp(char * str1, char * str2);
int strlen(char * str);
void trim(char * str);

#endif
