#ifndef PRODCONS_H
#define PRODCONS_H

#define MAX_PROD 15
#define MAX_CONS 15
#define PRIORITY 16
#define MEM_SPACE 4096
#define BACKGROUND 0
#define PIPE "pipe"

void initProdCons();
void intro();
void resetVariables();

void createProducer();
void producer();

void createConsumer();
void consumer();

#endif