#ifndef SHELL_H
#define SHELL_H

//Change this numner to whatever you feel like
#define MAX_COMMAND_LENGTH 1024
#define SHELL_STACK_SIZE 8192
#define SHELL_PRIORITY 0
#define SHELL_FOREGROUND 1

//some command defines
#define PCOUNT 11
#define HELP 0
#define TIME 1
#define PONG 2
#define PS  3
#define PRODCONS 4
#define CLEAR 5
#define EXIT 6
#define PIPEDEMO 7
#define MEMORYDEMO 8
#define WHILEONE 9
#define DOUBLELOCK 10

void shell_init();
int shell_execute(char *command);
int getCommand(char *command);
int isForeground(char * command);

#endif
