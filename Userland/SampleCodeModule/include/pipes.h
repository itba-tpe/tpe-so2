#include "syscalls.h"

#ifndef PIPES_H
#define PIPES_H

uint64_t open(char * name);

void close(char * name);

#endif