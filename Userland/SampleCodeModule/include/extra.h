#ifndef EXTRA_H
#define EXTRA_H

void whileOne(void);
void doubleLock(void);

int div(int n);
int ticksPerSecond(void);

#endif