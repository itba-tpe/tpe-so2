#include "shell.h"
#include "modules.h"
#include "string.h"
#include "stdio.h"
#include "syscalls.h"
#include "sync.h"
#include "pipeDemo.h"
#include "prodcons.h"
#include "memoryDemo.h"
#include "extra.h"

static mutex foreground;
static char * c[PCOUNT] = {"help", "time" , "pong" , "ps" , "prodcons", "clear", "exit", "pipeDemo", "memoryDemo", "whileOne", "doubleLock"};
static uint64_t p[PCOUNT] = {(uint64_t)&help, (uint64_t)&showTime, (uint64_t)&pong, (uint64_t)&ps, (uint64_t)&initProdCons, 0, 0, (uint64_t)&initPipesDemo, (uint64_t)&memoryDemo, (uint64_t)&whileOne, (uint64_t)&doubleLock};
//FIXME, add the address of prodcons when complete

void shell_init() {
	//Start Shell
	foreground = getMutex("foreground");
	wait(foreground);
	static char command[MAX_COMMAND_LENGTH];
	int exit = 0;
	printf("\nARQ TPE Group 2");
	printf("\nWhat module would you like to execute? (try 'help')");
	while (!exit) {
		printf("\n$>");
		scanf(command, MAX_COMMAND_LENGTH);
		exit = shell_execute(command);
	}
	printf("\nGoodbye.");
	return;
}

int shell_execute(char *command) {
	
    int exit = 0;
    
    //Now we need to compare the command to all the possible options
    if (*command == -1) {
        printf("\n^C");
        return exit;
    }
    
    int fg = isForeground(command);
    int id = getCommand(command);
    
    switch (id) {
            case HELP:
                os_create(c[id], p[id], 19, SHELL_STACK_SIZE, fg);
            break;
            
            case TIME:
                os_create(c[id], p[id], 19, SHELL_STACK_SIZE, fg);
            break;
            
            case PONG:
                if(fg == SHELL_FOREGROUND){
                    os_create(c[id], p[id], 19, SHELL_STACK_SIZE, fg);
                }else{
                    printf("\nPong not supported as a background process");
                }
            break;
            
            case PS:
                os_create(c[id], p[id], 19, SHELL_STACK_SIZE, fg);
            break;
            
            case PRODCONS:
               if(fg == SHELL_FOREGROUND){
                    os_create(c[id], p[id], 10, SHELL_STACK_SIZE, fg);
                }else{
                    printf("Prodcons not supported as a background process\n");
                }
            break;
            
            case CLEAR:
                if(fg == SHELL_FOREGROUND){
                    clear();
                    fg = !SHELL_FOREGROUND;
                }else{
                    //FIXME help with how to react against "clear &", and "exit &"
                    printf("\nMaybe clear shouldn't have &");
                }
            break;
            
            case EXIT:
                if(fg == SHELL_FOREGROUND){
                    exit = 1;
                    fg = !SHELL_FOREGROUND;
                }else{
                    printf("\nMaybe exit shouldn't have &");
                }
            break;

            case PIPEDEMO:
                if(fg == SHELL_FOREGROUND){
                   os_create(c[id], p[id], 10, SHELL_STACK_SIZE, fg);
                } else {
                   printf("\npipeDemo not supported as a background process\n");
                }
            break;

            case MEMORYDEMO:
                os_create(c[id], p[id], 10, SHELL_STACK_SIZE, fg);
            break;

            case WHILEONE:
                os_create(c[id], p[id], 19, 4096, fg);
            break;

            case DOUBLELOCK:
                os_create(c[id], p[id], 19, 4096, fg);
            break;
            
        default:
            printf("\nshell: ");
            printf(command);
            printf(": command not found (try help)");
            fg = !SHELL_FOREGROUND;
            break;
    }
    
    if(fg == SHELL_FOREGROUND){
        wait(foreground);
    }
    
    return exit;
    
}

//searches in command array if it matches
int getCommand(char* command){
    
    int exist = -1;          //if it matches an existing command
    
    //exist check
    for(int i = 0 ; i < PCOUNT && exist == -1 ; i++){
        if(strcmp(command, c[i]) == 0){
            exist = i;
        }
    }
    
    return exist;
}

//trims command and returns if it is foreground or background
int isForeground(char * command){
    int fg;             //if it is foreground or background
    
    //fg check (and cut)
    trim(command);
    int len = strlen(command) - 1;
    if(command[len] == '&'){
        fg = !SHELL_FOREGROUND;
        command[len] = '\0';
    }else{
        fg = SHELL_FOREGROUND;
    }
    trim(command);
    
    return fg;
}

