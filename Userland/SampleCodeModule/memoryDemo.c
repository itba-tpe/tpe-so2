#include "syscalls.h"
#include "stdio.h"
#include "memoryDemo.h"

void memoryDemo(){
	uint64_t pointers[MEM_ALLOCATED_AMOUNT];
	int i, j, mem = INITIAL_MEM;

	// Allocates MEM_ALLOCATED AMOUNT sections of memory with different sizes
	for (i = 0; i < MEM_ALLOCATED_AMOUNT; i++){
		if((pointers[i] = (uint64_t)os_malloc(mem + OFFSET)) == (uint64_t)-1){
			printf("\nERROR: Insufficient memory to allocate\n");
			return;
		} 
	}

	printf("\nAll pointers successfully created!\n");
	printf("\nComparing the pointers with one another...\n");

	// Compares to see that all the returned pointers are unique
	for (i = 0; i < MEM_ALLOCATED_AMOUNT - 1; i++){
		for (j = i + 1; j < MEM_ALLOCATED_AMOUNT; j++){
			if (pointers[i] == pointers[j]){
				printf("\nERROR: Pointers overlap!");
				for (i = 0; i < MEM_ALLOCATED_AMOUNT; i++){
					os_free((void *)pointers[i]);
				}
				return;
			}
		}
	}
	printf("\nSUCCESS: No pointers overlap!\n");

	printf("\nFreeing all pointers...\n");

	// Frees all the allocated memory
	for (i = 0; i < MEM_ALLOCATED_AMOUNT; i++){
		os_free((void *)pointers[i]);
	}
	printf("\nFinished.\n");

	return;
}
