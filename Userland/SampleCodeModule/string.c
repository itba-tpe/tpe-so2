#include "string.h"

int strcmp(char * str1, char * str2) {
	while(1) {
		if (*str1 != *str2) {
			return *str1 < *str2 ? -1 : 1;
		} else if (*str1 == '\0') {
			return 0;
		}
		str1++;
		str2++;
	}
}

void trim(char * str){
    int last = strlen(str) - 1;
    while(str[last] == ' '){
        str[last--] = '\0';
    }
    return;
}

int strlen(char * str) {
    int i = 0;
    while(*(str+i)) {
        i++;
    }
    return i;
}
