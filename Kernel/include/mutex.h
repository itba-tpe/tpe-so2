#ifndef MUTEX_H
#define MUTEX_H

#include "string.h"
#include "scheduler.h"
#include "memoryManager.h"

typedef struct mutex mutex;
typedef struct queueNode queueNode;
typedef struct listNode listNode;


//A simple node structure for a queue-like list
struct queueNode {
	int pid;				//process id of this process
	queueNode * next;		//the node of the process that will follow him after he is done with the mutex
};

//A simple structure for a mutex
struct mutex {
	char * name;			//name to identify the resource
	char value;				//value of the mutex
	queueNode * queue;		//queue of processes waiting blocked to access this mutex
};

//A simple structure for a list of mutexes
struct listNode {
	mutex * mutex;			//pointer to a mutex
	listNode * next;		//pointer to a node with another mutex
};


mutex * getMutex(char * name);

void deleteMutex(mutex * m);

mutex * findMutex(char * name);

void adquire(mutex * m);

void release(mutex * m);

int lockMutex(char * ptr);

int unlockMutex(char * ptr);

#endif