#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H

#include <stdint.h>
#include <math.h>
#include "vesaDriver.h"

#define MEM_SIZE 0x300000	//TO DEFINE  (If we know the end, its memEndAdress - memStartAdress)
#define PAGE_SIZE 0x1000
#define MEM_ARRAY_SIZE (MEM_SIZE/PAGE_SIZE)

#define YES 1
#define NO 0

typedef struct {
	char isAllocated;			//Whether the block is allocated or free
	uint64_t size;
} memBlock;

uint64_t firstFit (uint64_t pages);
uint64_t allocate (uint64_t size);
uint64_t free (uint64_t ptr);

void printMemory();
void printAllocationError(uint64_t size, uint64_t pages);
void printFreeError();

#endif