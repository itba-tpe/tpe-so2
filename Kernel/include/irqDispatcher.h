
#ifndef IRQDISPATCHER_H
#define IRQDISPATCHER_H

#define IRQ_AMOUNT 2

#include <stdint.h>

uint64_t irqDispatcher(int irq, uint64_t rsi);

#endif

