#ifndef IPC_H
#define IPC_H

#include <stdint.h>
#include "string.h"
#include "scheduler.h"
#include "vesaDriver.h"

#define MAX_BUFFER 1000	//Based on the scope of the project, this seems enough
#define MAX_PIPES 	20	//To aliviate some pressure on our memory and make faster response times for the write and read it will be static
#define SAVED		3	//Since we want to use the same read/write syscalls, we save the first 3 numbers


typedef struct pipe pipe;
typedef struct pidNode pidNode;

int open(char * id);
int findPipe(char * id);
int write(int idx, char * input, uint64_t bytes);
int read(int idx, char * buffer, uint64_t bytes);
void close(char * id);
int findFree(void);
#endif