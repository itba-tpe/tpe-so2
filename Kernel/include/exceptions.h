
#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <stdint.h>

#define ZERO_EXCEPTION_ID 0
#define INVALID_OPCODE_ID 6
#define EXCEPTIONS 2
#define REGISTERS 17

static void showException(int idx, uint64_t * stackpointer);
void exceptionDispatcher(int exception, uint64_t * stackpointer);

#endif

