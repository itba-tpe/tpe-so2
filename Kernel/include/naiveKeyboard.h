#ifndef NAIVEKEYBOARD_H
#define NAIVEKEYBOARD_H

#include <stdint.h>

uint64_t keyboard_handler(uint64_t rsi);
char getChar();
void addChar(char c);		//I would like to make this one private, see if i can put it inside .c file or something
void handleCtrl(unsigned int code); //same as above

#endif