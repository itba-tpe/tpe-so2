#ifndef STRING_H
#define STRING_H

#include "lib.h"
#include "memoryManager.h"
#include <stdint.h>

int strcmp(char * str1, char * str2);
char * duplicateString(const char * name);
int strlen(const char * str);

//testing functions
char* intToCharA(char* digits, uint64_t number);
int writeString(char * dir, char * name);
    
#endif
