
#ifndef SYSTEMCALLS_H
#define SYSTEMCALLS_H

#include <stdint.h>
#include "scheduler.h"
#include "mutex.h"

#define STDIN 0
#define STDOUT 1
#define SYS_AMOUNT 21 //FIXME
#define SYS_CREATE_CODE 12
#define VALID_SYS(s) (s > 0 && s < SYS_AMOUNT)

uint64_t sys_ticks(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_sec(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_read(uint64_t fd, uint64_t buffer, uint64_t size, uint64_t rcx, uint64_t r8);
uint64_t sys_write(uint64_t fd, uint64_t buffer, uint64_t size, uint64_t rcx, uint64_t r8);
uint64_t sys_time(uint64_t timeArray, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_pixel(uint64_t x, uint64_t y, uint64_t r, uint64_t g, uint64_t b);
uint64_t sys_clear(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_beep(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_unbeep(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_malloc(uint64_t size, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_free(uint64_t ptr, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_create_process(uint64_t name, uint64_t rip, uint64_t priority, uint64_t memSize, uint64_t foreground);
uint64_t sys_delete_process(uint64_t pid, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_wait(uint64_t resource, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_post(uint64_t resource, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_get_mutex(uint64_t name, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_delete_mutex(uint64_t resource, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_ps(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_open_pipe(uint64_t id, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
uint64_t sys_close_pipe(uint64_t id, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);

void loadSyscalls();
uint64_t syscallsDispatcher(uint64_t code, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t r9);

#endif /* SYSTEMCALLS_H */
