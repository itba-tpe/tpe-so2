#ifndef VESADRIVER_H
#define VESADRIVER_H

void putPixel(uint64_t x,uint64_t y, unsigned char r, unsigned char g, unsigned char b);

void clearAll();
void clearFrom(int x, int y);

void nextLine();
void fillRect(unsigned char startX, unsigned char startY, uint16_t width, uint16_t height, unsigned char r, unsigned char b, unsigned char c);
void printChar(unsigned char myChar, unsigned char r, unsigned char g, unsigned char b);
void printWhiteString(char *str);
void printString(char *str, unsigned char r, unsigned char g, unsigned char b);
void deleteChar();

void printDec(uint64_t value);
void printHexa(uint64_t value);
void printBase(uint64_t value, uint32_t base);
uint64_t intToBase(uint64_t value, char * buffer, uint32_t base);

#endif
