#include <stdint.h>
#include <systemCalls.h>
#include "naiveKeyboard.h"
#include "naiveClock.h"
#include "vesaDriver.h"
#include "soundDriver.h"
#include "time.h"
#include "memoryManager.h"
#include "ipc.h"
#include "scheduler.h"

typedef uint64_t (*syscall) (uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8);
static syscall sysVector[SYS_AMOUNT];

uint64_t sys_ticks(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	uint64_t * ticks = (uint64_t *)rdi;
	*ticks = ticks_elapsed();
	return *ticks;
}

uint64_t sys_sec (uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	uint64_t * sec = (uint64_t *)rdi;
	*sec = seconds_elapsed();
	return *sec;
}

//We will only read from the keyboard buffer for this project --> fd = 0 (stdin)
uint64_t sys_read(uint64_t fd, uint64_t buf, uint64_t size, uint64_t rcx, uint64_t r8){
	uint64_t bytesRead = 0;
	char * buffer = (char *)buf;

	char c;
	if (fd == STDIN){							//read from the keyboard buffer (stdin)
		while(size > 0 && (c = getChar())) {	//getChar returns 0 when the keboard buffer is empty
			buffer[bytesRead++] = c;
			size--;
		}
	} else if (fd >= SAVED) {
		bytesRead = read((int)fd, buffer, size);
	}
	return bytesRead;
}

//We will only write to the screen for this project --> fd = 1 (stdout)
uint64_t sys_write(uint64_t fd, uint64_t buf, uint64_t size, uint64_t rcx, uint64_t r8){
	uint64_t bytesWritten = 0;
	char * buffer = (char *)buf;

	if (fd == STDOUT) {
		while(size--) {
			char c = *buffer;
			if (c == '\n') {
				nextLine();
			} else if (c == '\b') {
				deleteChar();
			} else if (c == -1) {
				nextLine();
			} else {
				printChar(c,255,255,255);
			}
			buffer++;
			bytesWritten++;
		}
	} else if (fd >= SAVED) {
		bytesWritten = write((int)fd, buffer, size);
	}

	return bytesWritten;
}

uint64_t sys_time(uint64_t timeArray, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	//2 for hour, 2 for minutes, 2 for seconds
	uint64_t * time = (uint64_t *) timeArray;

	uint64_t hour = getHours();
	uint64_t min = getMinutes();
	uint64_t sec = getSeconds(); 

	switch(hour){
		case 0: hour = 21;
				break;
		case 1: hour = 22;
				break;
		case 2: hour = 23;
				break;
		default: hour -= 3;
	}

	time[0] = hour/10;
	time[1] = hour%10;
	time[2] = min/10;
	time[3] = min%10;
	time[4] = sec/10;
	time[5] = sec%10;

	return timeArray;
}

uint64_t sys_pixel(uint64_t x, uint64_t y, uint64_t r, uint64_t g, uint64_t b) {
	putPixel(x,y,(unsigned char)r, (unsigned char)g, (unsigned char)b);
	return 0;
}

//It clear the screen and goes back to original starting point
uint64_t sys_clear(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	clearAll();
	return 0;
}

//Play sound
uint64_t sys_beep(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	playSound();
	return 0;
}

//Stop sound
uint64_t sys_unbeep(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	stopSound();
	return 0;
}

uint64_t sys_malloc(uint64_t size, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	return allocate(size);
}

uint64_t sys_free (uint64_t ptr, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	return free(ptr);
}

uint64_t sys_create_process(uint64_t name, uint64_t rip, uint64_t priority, uint64_t memSize, uint64_t foreground) {
	createProcess(name, rip, priority, memSize, foreground);
	return 0;
}

uint64_t sys_wait(uint64_t resource, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	adquire((mutex *) resource);
	return 0;
}

uint64_t sys_post(uint64_t resource, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	release((mutex *) resource);
	return 0;
}

uint64_t sys_get_mutex(uint64_t name, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	return (uint64_t) getMutex((char *) name);
}

uint64_t sys_delete_mutex(uint64_t resource, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
	deleteMutex((mutex *) resource);
	return 0;
}

uint64_t sys_ps(uint64_t rdi, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8) {
    uint64_t * process = (uint64_t *)rdi;
    *process = listAllProcess();
    return *process;
}

uint64_t sys_open_pipe(uint64_t id, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8){
	return open((char *)id);
}

uint64_t sys_close_pipe(uint64_t id, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8){
	close((char *)id);\
	return 0;
}


void loadSyscalls(){
	sysVector[1] = sys_ticks;
	sysVector[2] = sys_sec;
	sysVector[3] = sys_read;
	sysVector[4] = sys_write;
	sysVector[5] = sys_time;
	sysVector[6] = sys_clear;
	sysVector[7] = sys_pixel;
	sysVector[8] = sys_beep;
	sysVector[9] = sys_unbeep;
	sysVector[10] = sys_malloc;
	sysVector[11] = sys_free;
	sysVector[12] = sys_create_process;
	sysVector[13] = sys_delete_process;
	sysVector[14] = sys_wait;
	sysVector[15] = sys_post;
	sysVector[16] = sys_get_mutex;
	sysVector[17] = sys_delete_mutex;
    sysVector[18] = sys_ps;
    sysVector[19] = sys_open_pipe;
    sysVector[20] = sys_close_pipe;
}

//todo why not return anything from a syscall? sys_read returns but return value is ignored by dispatcher (Maybe since its void it wont change rax, but that's just not pretty)
uint64_t syscallsDispatcher(uint64_t code, uint64_t rsi, uint64_t rdx, uint64_t rcx, uint64_t r8, uint64_t r9){ // lega en rdi desde asm
  if(VALID_SYS(code)){
  	return (sysVector[code])(rsi,rdx,rcx,r8,r9);
  }
  return 0;
}


