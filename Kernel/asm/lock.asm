;http://forum.codecall.net/topic/64301-writing-a-simple-lock-mutex-in-cassembly/

GLOBAL lockMutex
GLOBAL unlockMutex

section .text

lockMutex:
	push rbp
	mov rbp, rsp

	mov rax, 1
	xchg rax, [rdi]		;This commands does the magic

	mov rsp, rbp
	pop rbp
	ret

unlockMutex:
	push rbp
	mov rbp, rsp

	mov rax, 0
	xchg rax, [rdi]

	mov rsp, rbp
	pop rbp
	ret