
#include "scheduler.h"

//---------------------------------------------------
//creating some variables

static process processA[MAXPROCESS];
static scheduleNode * iter = NULL;

//for ps
static char pList[MAXSIZE] = {0};

static uint64_t pidC = 0;

static uint64_t totalNodes = 0;

//---------------------------------------------------

void scheduler_init(uint64_t rip){
    //clearing the used flag.f
    for(int i = 0; i < MAXPROCESS ; i++){
        processA[i].used = 0;
    }
    //create the first process
    createProcess((uint64_t) "init", rip, MAXPRIORITY, INIT_MEM, 0);
    processA[(iter->pid)%MAXPROCESS].state = ACTIVE;
    return;
}


uint64_t next(uint64_t curRSP){
    //upgrade old rsp
    processA[(iter->pid)%MAXPROCESS].rsp = curRSP;
    
    //If its dead, then we know it killed itself, so we only need to change to newHead (AKA iter = iter->next)
    if(processA[(iter->pid)%MAXPROCESS].state == DEAD) {
        //We mark the process slot as unused
        processA[(iter->pid)%MAXPROCESS].used = 0;
        //We save the address of the new head of the list
        scheduleNode * nextHead = iter->next;
        //We free this dummy node
        free((uint64_t) iter);
        //We update the list
        iter = nextHead;
    }
    //If it is Active, then set it as Ready, if it is Blocked, keep it blocked
    if(processA[(iter->pid)%MAXPROCESS].state == ACTIVE){
        processA[(iter->pid)%MAXPROCESS].state = READY;
    }
    
    //search for the next not-blocked process
    do {
        iter = iter->next;
    } while (processA[(iter->pid)%MAXPROCESS].state == BLOCKED);
    
    //setting this process as active
    processA[(iter->pid)%MAXPROCESS].state = ACTIVE;
    
    //We return it's rsp for the context switch
    return processA[(iter->pid)%MAXPROCESS].rsp;
}

uint64_t first() {
    return processA[(iter->pid)%MAXPROCESS].rsp;
}

uint64_t createProcess(uint64_t name, uint64_t rip, uint64_t priority, uint64_t memSize, uint64_t foreground){

    //find a "free" space in process array
    uint64_t iid = getFreeProcess();
    if (iid == -1){
        if (foreground == 1) {
            nextLine();
            printWhiteString("You exceeded the 20 process limit. Process was not created");
            mutex * m = getMutex("foreground");
            release(m);
        } else {
            nextLine();
            printWhiteString("You exceeded the 20 process limit. Process was not created");
        }
        return -1;  //this means that max process was exceeded
    }
    
    processA[iid].pid = getPid(iid);
    processA[iid].name = (char *) name;
    processA[iid].priority = priority;
    processA[iid].resMemory = memSize;
    
    //Create empty stack
    uint64_t stack = allocate(memSize);
    //If we are out of memory to allocate this new process, abort.
    if(stack == -1){
        processA[iid].used = 0;
        return -1;
    }

    //Fill the stack with the registers (Values taken from Wyrm's Bitbucket)
    //Since a stack works from the top to the bottom, we get the upper address
    uint64_t stackUpper = stack + memSize - 1;

    //Now we make space for the stackFrame and add its values
    stackFrame * frame = (stackFrame *) (stackUpper - sizeof(stackFrame) + 1);
    
    //Registers
    frame->rax = 0x000;
    frame->r15 = 0x000;
    frame->r14 = 0x000;
    frame->r13 = 0x000;
    frame->r12 = 0x000;
    frame->r11 = 0x000;
    frame->r10 = 0x000;
    frame->r9 = 0x000;
    frame->r8 = 0x000;
    frame->rsi = 0x000;
    frame->rdi = rip;
    frame->rbp = 0x000;
    frame->rdx = 0x000;
    frame->rcx = 0x000;
    frame->rbx = 0x000;

    //IretQ
//    frame->rip = rip;
    frame->rip = (uint64_t) &runProcess;
    frame->cs = 0x008;
    frame->eflags = 0x202;
    frame->rsp = (uint64_t) &(frame->base);
    frame->ss = 0x000;
    frame->base = 0x000;

    //Once the stack is properly filled
    processA[iid].rsp = (uint64_t) frame;
    processA[iid].stack = stack;
    processA[iid].foreground = foreground;

    //If it's a foreground process
    processA[iid].state = READY;
    
    int quantity = addQuantity(priority);

    //If this is the first process we simply make a round list and point inter to any of its elements, cuz they are all the same

    if (iter == NULL) {

        //We create the first one
        scheduleNode * first = (scheduleNode *) allocate(sizeof(scheduleNode));
        first->pid = processA[iid].pid;
        first->next = first;
        scheduleNode * previous = first;
        for (int i = 0; i < (quantity-1); ++i) {
            //We create the new node
            scheduleNode * node = (scheduleNode *) allocate(sizeof(scheduleNode));
            node->pid = processA[iid].pid;
            node->next = previous->next;
            previous->next = node;
            previous = node;
        }

        //Now we update global variables
        iter = first;
    } else {
        //We calculate the interval
        int interval = (totalNodes/quantity) + 1;
        int times = quantity;
        int add = 0;
        //iter = addRec(iter, processA[iid].pid, interval, 0, quantity);
        scheduleNode * current = iter;
        while(times > 0) {
            if (add == 0) {
                //Add a new node
                scheduleNode * newNode = (scheduleNode *) allocate(sizeof(scheduleNode));
                if ((uint64_t) newNode == -1) {
                    //printWhiteString("ERROR");
                    processA[iid].used = 0;
                    return -1;
                }
                newNode->pid = processA[iid].pid;
                newNode->next = current->next;
                current->next = newNode;

                //We added once
                times--;

                //Go to next node
                current = newNode->next;
            } else {
                //Go to next node
                current = current->next;
            }

            //Update add variable (to see if we add a node or not)
            add = (add + 1) % interval;
        }
    }

    //Update node count
    totalNodes += quantity;

    //Once we finish we return
    return processA[iid].pid; //FIXME discuss if return 0 should be a thing -> E: what should be then? (ATM returns pid)
}



//FIXME freeProcess should return 0 only if the pid != currPid, 1 otherwise (This is to let the syscall know if it has to do a context switch or not) -> E:it does
uint64_t freeProcess(uint64_t pid){
    //all the function should be atomic (that's why we call cli before we call this function on the syscall)
    
    if(iter == NULL){
        return 1;
    }

    int currPid = iter->pid;

    scheduleNode * nextHead = NULL;
    scheduleNode * current = iter->next;
    scheduleNode * previous = iter;
    int deleted = 0;
    int foundFirst = 0;

    for (int i = 0; i < totalNodes; ++i) {

        if (current->pid == pid) {
            scheduleNode * aux = current->next;
            previous->next = aux; //We bridge the nodes
            free((uint64_t)current);
            current = aux;
            deleted++;
        } else {
            if (!foundFirst) {
                foundFirst = 1;
                nextHead = current;
            }
            previous = current;
            current = current->next;
        }
    }

    totalNodes -= deleted;

    //We free the space allocated to this process's stack
    free((uint64_t) processA[pid%MAXPROCESS].stack);

    //If the process is killing itself, we make a dummy node so next() wont overwrite some other process's rsp
    if (currPid == pid) {
        scheduleNode * node = (scheduleNode *) allocate(sizeof(scheduleNode));
        node->pid = pid;
        node->next = nextHead;

        iter = node;
        processA[pid%MAXPROCESS].state = DEAD;

        //If it was a foreground process, we post to the foreground mutex
        if(processA[pid%MAXPROCESS].foreground == 1) {
            mutex * m = getMutex("foreground");
            release(m);
        }

        return 1;
    }

    //If it was a foreground process, we post to the foreground mutex, this would be the case a background process kills a foreground process that is blocking shell
    if(processA[pid%MAXPROCESS].foreground == 1) {
        mutex * m = getMutex("foreground");
        release(m);
    }

    //Else we just mark its process slot as unused
    processA[pid%MAXPROCESS].used = 0;
    return 0;
}


uint64_t listAllProcess(){
    
    uint64_t iterInt = 0;
    char* firstRow = "\npid\tname\tpriority\treserved memory\tstate\n";
    char intChar[20];

    iterInt += writeString(pList+iterInt, firstRow);

    //modify accordingly
    for(int i = 0 ; i < MAXPROCESS ; i++){
        if(processA[i].used == 1){
            //Verify we have enough space on our buffer for the next line
            if (MAXSIZE - iterInt - 1 < MAXLINE) {
                pList[iterInt] = '\0';
                return (uint64_t) pList;
            }
            iterInt += writeString(pList+iterInt, intToCharA(intChar, processA[i].pid) );
            iterInt += writeString(pList+iterInt, "\t");
            iterInt += writeString(pList+iterInt, processA[i].name);
            iterInt += writeString(pList+iterInt, "\t");
            iterInt += writeString(pList+iterInt, intToCharA(intChar, processA[i].priority) );
            iterInt += writeString(pList+iterInt, "\t");
            iterInt += writeString(pList+iterInt, intToCharA(intChar, processA[i].resMemory) );
            iterInt += writeString(pList+iterInt, "\t");

            switch (processA[i].state) {

                    case ACTIVE:
                    iterInt += writeString(pList+iterInt, "ACTIVE");
                    break;

                    case READY:
                    iterInt += writeString(pList+iterInt, "READY");
                    break;

                    case BLOCKED:
                    iterInt += writeString(pList+iterInt, "BLOCKED");
                    break;

                    //shouldnt exist, but added anyways as it helps in debugging
                    case DEAD:
                    iterInt += writeString(pList+iterInt, "DEAD");
                    break;

                default:
                    iterInt += writeString(pList+iterInt, "?");
                    break;
            }
            
            iterInt += writeString(pList+iterInt, "\t");

            if(processA[i].foreground){
                iterInt += writeString(pList+iterInt, "FOREGROUND");
            }else{
                iterInt += writeString(pList+iterInt, "BACKGROUND");
            }

            iterInt += writeString(pList+iterInt, "\n");
        }
    }

    pList[iterInt] = '\0';
    
    return (uint64_t) pList;
}

int changeState(uint64_t pid,char state){
    
    if(processA[pid%MAXPROCESS].pid == pid){
        processA[pid%MAXPROCESS].state = state;
        return 0;
    }
    
    return -1;
}

//---------------------------------------------------
//realm of internal functions

void runProcess(uint64_t entryPoint){
    void (*function)(void) = (void (*)(void)) entryPoint;
    function();
    _killProcess(iter->pid);
    //if a process returned a value, it means that it is the current process
    return;
}

//returns the iid of a free process. else it returns -1 (there are more than MAXPROCESS processes)
uint64_t getFreeProcess(void){
    
    uint64_t iid = -1;
    
    for(int i = 0 ; iid == -1 && i < MAXPROCESS ; i++){
        //this should be atomic
        if(processA[i].used == 0){
            iid = i;
            processA[i].used = 1;
        }
        //from here on atomic or not should not matter
    }
    
    return iid;
}

//The way we use this function, we know currentNode cannot be NULL
scheduleNode * addRec(scheduleNode * currentNode, uint64_t pid, int interval,int currentOrder, unsigned int times){
    //Base case
    if(times == 0){
        //shouldnt happen if currentNode is NULL, as times should be called with a non-zero positive value
        return currentNode;
    }

    //We check if its time to add a new node, or not, currentOrder will tell us that
    if (currentOrder == 0) {

        //We add this new node as the next of the current node
        scheduleNode * node = (scheduleNode *) allocate(sizeof(scheduleNode));
        node->pid = pid;
        node->next = currentNode->next;
        currentNode->next = node;

        //Now we add more until times == 0
        node->next = addRec(node->next, pid, interval, (currentOrder + 1) % interval, --times);
    } else {

        //We add more until times == 0
        currentNode->next = addRec(currentNode->next, pid, interval, (currentOrder + 1) % interval, times);
    }

    return currentNode;
}

uint64_t addQuantity(uint64_t priority){
    if(priority >= MAXPRIORITY){
        return 1;
    }
    return (MAXPRIORITY-priority);
}


//Block the process with said pid
int block (int pid) {
    return changeState(pid, BLOCKED);
}

//Unblock process with said pid
int unblock (int pid) {
    return changeState(pid, READY);
}

//Get pid of the process that's running right now
int getRunningPid() {
    return processA[(iter->pid)%MAXPROCESS].pid;
}

//Kill a process by sending pid
void _killProcess(uint64_t pid) {
    sys_delete_process(pid, 0, 0, 0, 0);
}

uint64_t getPid(uint64_t iid){
    
    //increases pidC until its remainder is iid
    while( ((++pidC)%MAXPROCESS) != iid );
    
    return pidC;
}
