#include "time.h"
#include "naiveKeyboard.h"
#include "irqDispatcher.h"

static uint64_t (*irqVector[IRQ_AMOUNT])(uint64_t rsi) = {timer_handler, keyboard_handler};

uint64_t irqDispatcher(int irq, uint64_t rsi) {
	return (*irqVector[irq])(rsi); 
}
