#include "memoryManager.h"

static void * const memStartAddress = (void *)0x900000;		
static memBlock memory[MEM_ARRAY_SIZE] = {{0, MEM_ARRAY_SIZE}, {0, 0}};

// Send the number of pages needed for allocation
uint64_t firstFit (uint64_t pages) {
	uint64_t index;

	for (index = 0; index < MEM_ARRAY_SIZE; index++) {
		
		if (memory[index].size >= pages) {
			if (memory[index].isAllocated == 0) {
				return index;
			}
		}
	}
	
	return -1; 	//no space to allocate required memory
}

uint64_t allocate (uint64_t size){
	//Verify if input is legal
	if (size <= 0 || size > MEM_SIZE) {
		return -1;
	}

	//Pages needed to fullfill this size request
	uint64_t pages = size / PAGE_SIZE;
	if (size % PAGE_SIZE != 0) {		//C rounds down, so we round up.
		pages++;
	}


	//Calling firstfit to retrive the address to then return
	uint64_t startIdx = firstFit(pages);
	if (startIdx == -1) {
		printAllocationError(size, pages);
		return -1;	//If we have no space for this new request, we return an error
	}

	//We save the original size of the block
	uint64_t blockSize = memory[startIdx].size;

	//Setting the block to be allocated as allocated for future calls
	uint64_t i;
	for (i = startIdx; i < (startIdx + pages); i++) {
		memory[i].isAllocated = YES;
		memory[i].size = 0;
	}
	//Only the first page of a block should hold the size of the block
	memory[startIdx].size = pages;		//The block we just allocated

	//If the original block was bigger than what we allocated
	if (blockSize > pages) {
		memory[startIdx + pages].size = blockSize - pages;	// Amount of continous free blocks left after allocation
		memory[startIdx + pages].isAllocated = NO;			// Redundant
	}

	//Now we get the physical address of the block we just allocated and return it
	uint64_t returnPtr = (uint64_t)(memStartAddress + startIdx*PAGE_SIZE);
	return returnPtr;
}

uint64_t free (uint64_t ptr) {
	//We verify if the input is legal / todo verify id the second condition is valid (memstart + MEMSIZE)
	if (ptr < (uint64_t) memStartAddress || ptr > (uint64_t) (memStartAddress + (MEM_ARRAY_SIZE - 1)*PAGE_SIZE)) {
		return -1;
	}


	//We get the page corresponding to this memoryAdress and we verify if it's the first page of a block
	uint64_t startIdx = (ptr - (uint64_t)memStartAddress)/PAGE_SIZE;		//Dangerous if what i sent to free is not something we returned
	
	if ((ptr - (uint64_t)memStartAddress)%PAGE_SIZE != 0) {
		printFreeError();
		return -1;
	}

	uint64_t blockSize = memory[startIdx].size;

	if (blockSize == 0){
		printFreeError();
		return -1;		//We will not free a block unless the pointer we returned on malloc is sent on free
	}

	//If its valid, we unallocate the block
	uint64_t i;
	for (i = startIdx; i < (startIdx + blockSize); i++) {
		memory[i].isAllocated = NO;
		//memory[i].size = 0;		//redundant
	}

	//Now we merge with free blocks next to the block we just unallocated

	//To the right
	uint64_t nextBlockIdx = startIdx + blockSize;

	if (nextBlockIdx < MEM_ARRAY_SIZE) { //We make sure a next block exists
		if (memory[nextBlockIdx].isAllocated == NO){
			memory[startIdx].size = blockSize + memory[nextBlockIdx].size;
			memory[nextBlockIdx].size = 0;
		}
	}

	//To the left (if we are not the first block)
	if (startIdx > 0) {
		//If the last page of the block is allocated, there is no need to go to the beginning page of the block.
		if (memory[startIdx - 1].isAllocated == NO) {
			//Now we go to the first page to make it the new first page of our merged block
			uint64_t i = startIdx;
			do {
				i--;
			} while (memory[i].size == 0);

			//We assign the new size of the block
			memory[i].size = memory[i].size + memory[startIdx].size;
			memory[startIdx].size = 0;	//We overwrite the old size with the appropiate zero
		}

	}

	//If we got here, we are golden
	return 0;
}

void printMemory() {
	uint64_t index;
	printWhiteString("ST:");
	for (index = 0; index < MEM_ARRAY_SIZE; ++index) {
		if (memory[index].size != 0) {
			printDec(index);
			printWhiteString("->");
			printWhiteString("[");
			printDec(memory[index].size);
			printWhiteString(",");
			printDec(memory[index].isAllocated);
			printWhiteString("] / ");
		}
	}
	printWhiteString("END");
}

void printAllocationError(uint64_t size, uint64_t pages){
	nextLine();
	printWhiteString("Size: ");
	printDec(size);
	printWhiteString(", Pages: ");
	printDec(pages);
	printWhiteString(", memDump: ");
	nextLine();
	printMemory();
	nextLine();
}

void printFreeError(){
	char * errorMsg = "\nFAILURE TO FREE\n";
	printWhiteString(errorMsg);
}
