#include <time.h>

static unsigned long ticks = 0;

uint64_t timer_handler(uint64_t rsi) {
	if(ticks == 0) {
		ticks++;
		return first();
	}
	ticks++;
	return next(rsi);
}

uint64_t ticks_elapsed() {
	return ticks;
}

uint64_t seconds_elapsed() {
	return ticks / 18;
}
