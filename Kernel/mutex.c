#include "mutex.h"

//Header or first node of this list of mutexes
static listNode * myMutexes = NULL;

//We return the pointer to a mutex, so be it because it already existed or because we created it just now.
mutex * getMutex(char * name) {
	
	//First we check if we can find this mutex already on our list
	mutex * aux = NULL;
	aux = findMutex(name);

	//If we couldn't find it we create one
	if (aux == NULL) {
		//We reserve the space for the mutex and it's node
		mutex * newMutex = (mutex *) allocate(sizeof(mutex));

		//Verify it was sucessful
		if (newMutex == (mutex *) -1) {
			return (mutex *) -1;
		}

		//Now its node
		listNode * newNode = (listNode *) allocate(sizeof(listNode));

		//Verify
		if (newNode == (listNode *) -1) {
			free((uint64_t)newMutex);
			return (mutex *) -1;
		}

		//Now we gives the new mutex some values
		newMutex->name = duplicateString(name);
		if(newMutex->name == (char *) -1) {
			free((uint64_t)newNode);
			free((uint64_t)newMutex);
			return (mutex *) -1;
		}
		newMutex->value = 0;
		newMutex->queue = NULL;

		//Finally we add it to our list of mutexes
		newNode->mutex = newMutex;
		newNode->next = myMutexes;
		myMutexes = newNode;

		//And we return our new Mutex
		aux = newMutex;
	}

	//now we return the mutex
	return aux;
}

//Deletes mutex and all it's resources (up to the user to actually send to pointer to a proper mutex)
void deleteMutex(mutex * m) {

	//First we search it on the list of mutexes
	listNode * current = myMutexes;
	listNode * previous = NULL;
	while(current != NULL && (current->mutex) != m) {
		previous = current;
		current = current->next;
	}

	//Then we remove it
	if (current != NULL && previous == NULL) {
		myMutexes = current->next;
	} else if (current != NULL && previous != NULL) {
		previous->next = current->next;
	} else {
		//The mutex was not on our list, so it is not our job to deal with it
		return;
	}

	//Now we free all its resources

	free((uint64_t)current);	//The listNode
	free((uint64_t)(m->name));	//The name

	while(m->queue != NULL) {
		queueNode * next = (m->queue)->next;
		_killProcess((m->queue)->pid);
		free((uint64_t)(m->queue));		//The queueNode
		m->queue = next;
	}

	free((uint64_t)m);		//The mutex

	return;
}

//This should be internal, it tries to find a mutex with the same name as the argument and return its pointer
mutex * findMutex(char * name) {

	//A simple search using strcmp
	listNode * current = myMutexes;
	while(current != NULL) {
		if (strcmp((current->mutex)->name, name) == 0) {
			return current->mutex;
		}
		current = current->next;
	}

	return NULL;
}

//Adquire a mutex
void adquire(mutex * m) {
	
	//We adquire the lock, if lockMutex returns 0 then we sucessfully adquired it, otherwise we block ourselves and wait to be unblocked by a mutex release
	if (lockMutex(&(m->value))) {
		//We figue out who we are and then add ourselves to the waiting queue (at the end of it, of course)
		int pid = getRunningPid();
		queueNode * p = (queueNode *) allocate(sizeof(queueNode));
		
		if (p == (queueNode *) -1) {
			return;		//FIXME WTF do we do here?
		}

		p->pid = pid;
		p->next = NULL;

		if (m->queue == NULL) {
			m->queue = p;
		} else {
			queueNode * current = m->queue;

			while((current->next) != NULL) {
				current = current->next;
			}

			current->next = p;
		}

		//Now we block ourselves and change context
		block(pid);
		_contextSwitch();
	}

	return;
}

void release(mutex * m) {

	//If there are other processes blocked waiting for this mutex we dont have to change the value of the mutex, just unblock the next one and return

	if (m->queue != NULL) {

		//We need to be careful of processes that are in the queue that no longer exist (they got SIGTERM while waiting, what a sad way to die)
		while ( (m->queue) != NULL && unblock((m->queue)->pid) == -1) {
			//Lets free this node and try with the next one
			queueNode * next = (m->queue)->next;
			free((uint64_t)(m->queue));
			m->queue = next;
		}

		//If we sucessfully unblocked a non-dead process from the queue, we remove it from the queue
		if ((m->queue) != NULL) {
			queueNode * next = (m->queue)->next;
			free((uint64_t)(m->queue));
			m->queue = next;
		} else {
			//All the processes in the queue were dead, so we just unlock the mutex
			unlockMutex(&(m->value));
		}
	} else {
		//If the queue is empty we change the value of the mutex and return
		unlockMutex(&(m->value));
	}

	return;
}