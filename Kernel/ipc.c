#include "ipc.h"
#include "vesaDriver.h"

//Structure that hold the buffer to which we are going to write and read from and some type of identifier the programmer decides
struct pipe {
	int write;
	int read;
	char * id;
	pidNode * queue;
	char used;
	int avail;
	char buffer[MAX_BUFFER];
};

struct pidNode {
	int pid;
	pidNode * next;
};

//Our pipes, all the data will be initialized with zeros so no problems here m8
static pipe myPipes[MAX_PIPES] = {{0}};

//We get the int for the pipe with this id
int open(char * id) {

	//We search for the pipe with indicated id on our list
	int i = findPipe(id);

	//If we couldn't find it we just create it
	if (i == -1) {

		//We search for a free space in our array
		i = findFree();

		//If there is no space left, we just return -1
		if (i == -1) {
			return -1;
		}

		//Otherwise, we load our info into this process structure

		//We duplicate the string
		myPipes[i].id = duplicateString(id);

		if ((uint64_t) myPipes[i].id == -1) {
			return -1;
		}

		//We initialize the pipe
		(myPipes[i].buffer)[0] = '\0';	//Empty the buffer
		myPipes[i].used = 1;			//Set it as used
		myPipes[i].write = 0;			//Next to write is on 0
		myPipes[i].read = 0;			//Next to read is on 0
		myPipes[i].avail = MAX_BUFFER;	//We have all the space in the buffer TO WRITE
		myPipes[i].queue = NULL;		//No process blocked waiting for this resource
	}

	//We return the user the int that represents this pipe
	return i + SAVED;	//We add SAVED so the user uses 0, 1 and 2 for stdout, stdin and stderr
}

//Internal function
int findPipe(char * id) {

	//A simple search using strcmp
	for (int i = 0; i < MAX_PIPES; ++i) {
		if (myPipes[i].used != 0) {
			if (strcmp(myPipes[i].id, id) == 0) {
				return i;
			}
		}
	}

	return -1;
}

//Write to the buffer of the pipe
int write(int idx, char * input, uint64_t bytes) {

	//We change the idx to the position in the array
	idx = idx - SAVED;

	//We check the pipe is valid
	if (idx < 0 || idx >= MAX_PIPES) {
		return -1;
	} else {
		if (myPipes[idx].used == 0) {
			return -1;
		}
	}

	//We check how much space in the buffer we have

	//If we are tying to write more than we can take, we dont write at all and notify the user, so the previous data is safe.
	//(In theory they still have this input available to try again later, but we cant say the same for the data already in the pipe)
	if (bytes > myPipes[idx].avail) {
		return -1;
	}

	//Otherwise, we write to the buffer
	int write = myPipes[idx].write;
	for (int i = 0; i < bytes; ++i) {
		(myPipes[idx].buffer)[write] = input[i];
		//printChar(myPipes[idx].buffer[write], 255, 255, 255);
		write = (write + 1) % MAX_BUFFER;
		myPipes[idx].avail--;
	}

	myPipes[idx].write = write;

	//Before we return we notify the first process in the queue that he can read something now
	while ( (myPipes[idx].queue) != NULL && unblock((myPipes[idx].queue)->pid) == -1) {
		//Lets free this node and try with the next one, because this one is dead
		pidNode * next = (myPipes[idx].queue)->next;
		free((uint64_t)myPipes[idx].queue);
		myPipes[idx].queue = next;
	}

	//If we sucessfully unblocked a non-dead process from the queue, we remove it from the queue
	if (myPipes[idx].queue != NULL) {
		pidNode * next = (myPipes[idx].queue)->next;
		free((uint64_t)(myPipes[idx].queue));
		myPipes[idx].queue = next;
	}
	
	return bytes;
}

//Read from the buffer of the pipe
int read(int idx, char * buffer, uint64_t bytes) {

	//We change the idx to the position in the array
	idx = idx - SAVED;

	//We check the pipe is valid
	if (idx < 0 || idx >= MAX_PIPES) {
		return -1;
	} else {
		if (myPipes[idx].used == 0) {
			return -1;
		}
	}

	//If there are other proccesses waiting to read from this pipe, since we are polite, we wait until they are finished
	if (myPipes[idx].queue != NULL) {

		//Who are we?
		int pid = getRunningPid();

		//We add ourselves to the queue
		pidNode * node = (pidNode *) allocate(sizeof(pidNode));
		node->pid = pid;
		node->next = NULL;

		pidNode * current = myPipes[idx].queue;
		while(current->next != NULL) {
			current = current->next;
		}
		current->next = node;

		block(pid);
		_contextSwitch();
	}

	//Just a quick check that the pipe is still open
	if (myPipes[idx].used == 0) {
		return -1;
	}

	//If there is nothing to read we wait until there is
	if (myPipes[idx].write == myPipes[idx].read) {
		//Who are we?
		int pid = getRunningPid();

		//We add ourselves to the queue
		pidNode * node = (pidNode *) allocate(sizeof(pidNode));
		node->pid = pid;
		node->next = NULL;

		pidNode * current = myPipes[idx].queue;
		
		if (current == NULL) {
			myPipes[idx].queue = node;

		}
	
		block(pid);
		_contextSwitch();
	}

	//Just a quick check that the pipe is still open
	if (myPipes[idx].used == 0) {
		return -1;
	}

	//We read up to bytes (or until we run out of stuff to read)
	int write = myPipes[idx].write;
	int read = myPipes[idx].read;

	int count = 0;

	for (int i = 0; i < bytes && read != write; ++i, ++count) {
		buffer[i] = (myPipes[idx].buffer)[read];
		read = (read + 1) % MAX_BUFFER;
		myPipes[idx].avail++;
	}

	myPipes[idx].read = read;

	//If i left things to be read i unblock the next process

	if (myPipes[idx].read != myPipes[idx].write) {

		//Before we return we notify the first process in the queue that he can read something now
		while ( (myPipes[idx].queue) != NULL && unblock((myPipes[idx].queue)->pid) == -1) {
			//Lets free this node and try with the next one, because this one is dead
			pidNode * next = (myPipes[idx].queue)->next;
			free((uint64_t)myPipes[idx].queue);
			myPipes[idx].queue = next;
		}

		//If we sucessfully unblocked a non-dead process from the queue, we remove it from the queue
		if (myPipes[idx].queue != NULL) {
			pidNode * next = (myPipes[idx].queue)->next;
			free((uint64_t)(myPipes[idx].queue));
			myPipes[idx].queue = next;
		}
	}

	return count;
}

//Frees a slot for a new pipe
void close(char * id) {

	//We find the pipe
	int i = findPipe(id);

	//If we actually found something
	if (i != -1) {

		//We mark it as no longer used
		myPipes[i].used = 0;

		//We free the space allocated to it's identifier
		free((uint64_t) myPipes[i].id);

		//We unblock all queued nodes
		pidNode * current = myPipes[i].queue;

		while(current != NULL) {
			int pid = current->pid;
			_killProcess(pid);
			pidNode * next = current->next;
			free((uint64_t) current);
			current = next;
		}
	}

	//Else we just return
	return;
}

//Returns i of free slot for the pipe
int findFree(void){
    
   	int idx = -1;
    
    for(int i = 0 ; idx == -1 && i < MAX_PIPES ; i++){
        if(myPipes[i].used == 0){
            myPipes[i].used = 1;
            idx = i;
        }
    }
    
    return idx;
}
